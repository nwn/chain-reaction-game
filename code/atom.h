#ifndef ATOM_H
#define ATOM_H

#include <SFML/Graphics.hpp>

namespace vi
{
    class Atom
    {
        public:
            Atom(sf::Vector2f pos, unsigned int dir);
            ~Atom();

            void update(const sf::Time deltaTime, const sf::Vector2f winSize);

            const sf::Vector2f getPosition() const;
            const float getRadius() const;
            const sf::CircleShape getCircle() const;

            static float Radius;
        private:
            sf::CircleShape mCircle;
            float mSpeedX, mSpeedY;
    };
}

#endif // ATOM_H
