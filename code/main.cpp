#include "util.h"
#include "level.h"
#include "atom.h"
#include "explosion.h"

#include <SFML/Graphics.hpp>

#include <vector>
#include <iostream>
#include <cstdlib>
#include <ctime>

int main()
{
    srand(time(NULL));
    sf::RenderWindow Window(sf::VideoMode(620, 400, 32), "SFML Template");
    sf::Font mono;
    if(!mono.loadFromFile("data/mono.ttf"))
        std::cerr << "Error: Cannot load font file" << std::endl;
    sf::Text score("", mono, 16);
    score.setPosition(5, 5);
    score.setColor(sf::Color(255, 255, 255, 200));

    sf::Clock clock;
    vi::Level level;
    bool clicked = false;
    unsigned int highScore = 0;
    while (Window.isOpen())
    {
        sf::Event Event;
        while (Window.pollEvent(Event))
        {
            switch(Event.type)
            {
                case sf::Event::Closed:
                    Window.close();
                    break;
                case sf::Event::KeyPressed:
                    switch(Event.key.code)
                    {
                        case sf::Keyboard::Escape:
                            Window.close();
                            break;
                        case sf::Keyboard::F5:
                            level = vi::Level();
                            clicked = false;
                            break;
                        default:
                            break;
                    }
                    break;
                case sf::Event::MouseButtonPressed:
                    switch(Event.mouseButton.button)
                    {
                        case sf::Mouse::Left:
                            if(!clicked)
                                level.addExplosion(sf::Mouse::getPosition(Window));
                            clicked = true;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        if(clock.getElapsedTime().asSeconds() > 0.015)
        {
            Window.clear();
            level.update(clock.restart(), Window);
            highScore = std::max(highScore, level.getScore());
            score.setString("Your Score: " + vi::util::toString(level.getScore()) +
                            "\nHigh Score: " + vi::util::toString(highScore));
            Window.draw(score);
            Window.display();
        }
    }

    return 0;
}
