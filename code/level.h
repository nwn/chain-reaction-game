#ifndef LEVEL_H
#define LEVEL_H

#include "atom.h"
#include "explosion.h"

#include <SFML/Graphics.hpp>

#include <vector>
#include <cmath>
#include <cstdlib>

namespace vi
{
    class Level
    {
        public:
            Level();
            ~Level();

            void update(sf::Time deltaTime, sf::RenderWindow& window);
            void addExplosion(sf::Vector2i pos);
            const unsigned int getScore() const;

        private:
            float mWidth;
            float mHeight;
            unsigned int mAtomCount;
            unsigned int mScore;

            std::vector<vi::Atom> mAtoms;
            std::vector<vi::Explosion> mExplosions;

            void checkCollisions();
    };
}

#endif // LEVEL_H
