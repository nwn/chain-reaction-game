#ifndef UTIL_H
#define UTIL_H

#include <sstream>

namespace vi
{
    namespace util
    {
        template <class type>
        const std::string toString(const type val)
        {
            std::stringstream ss;
            ss << val;
            return ss.str();
        }
    }
}

#endif // UTIL_H
