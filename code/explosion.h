#ifndef EXPLOSION_H
#define EXPLOSION_H

#include <SFML/Graphics.hpp>


namespace vi
{
    class Explosion
    {
        public:
            Explosion(sf::Vector2f pos);
            ~Explosion();

            const bool update(sf::Time deltaTime);

            const sf::Vector2f getPosition() const;
            const float getRadius() const;
            const sf::CircleShape getCircle() const;

        private:
            sf::CircleShape mCircle;
            sf::Time mAge;
            sf::Time mLifespan;
    };
}

#endif // EXPLOSION_H
