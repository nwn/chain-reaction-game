#include "level.h"

vi::Level::Level():
    mWidth(620),
    mHeight(400),
    mAtomCount(32)
{
    mScore = 0;

    for(unsigned int i = 0; i < mAtomCount; ++i)
    {
        mAtoms.emplace_back(sf::Vector2f(rand() % int(mWidth - mWidth/5) + mWidth/10, rand() % int(mHeight - mHeight/5) + mHeight/10), rand() % 4);
    }
}

vi::Level::~Level()
{
    //dtor
}

void vi::Level::update(sf::Time deltaTime, sf::RenderWindow& window)
{
    for(unsigned int i =0; i < mAtoms.size(); ++i)
    {
        mAtoms[i].update(deltaTime, sf::Vector2f(mWidth, mHeight));
    }
    for(unsigned int i = 0; i < mExplosions.size(); ++i)
    {
        if(!mExplosions[i].update(deltaTime))
        {
            mExplosions.erase(mExplosions.begin() + i);
        }
    }
    checkCollisions();
    for(unsigned int i = 0; i < mAtoms.size(); ++i)
    {
        window.draw(mAtoms[i].getCircle());
    }
    for(unsigned int i = 0; i < mExplosions.size(); ++i)
    {
        window.draw(mExplosions[i].getCircle());
    }
}

void vi::Level::addExplosion(sf::Vector2i pos)
{
    mExplosions.emplace_back(sf::Vector2f(pos.x, pos.y));
}

const unsigned int vi::Level::getScore() const
{
    return mScore;
}

void vi::Level::checkCollisions()
{
    std::vector<unsigned int> colliders;
    for(unsigned int i = 0; i < mAtoms.size(); ++i)
    {
        for(unsigned int j = 0; j < mExplosions.size(); ++j)
        {
            float distance = std::hypot(mExplosions[j].getPosition().x - mAtoms[i].getPosition().x,
                                        mExplosions[j].getPosition().y - mAtoms[i].getPosition().y);
            if(distance <= mAtoms[i].getRadius() + mExplosions[j].getRadius())
            {
                colliders.push_back(i);
                break;
            }
        }
    }
    for(unsigned int i = 0; i < colliders.size(); ++i)
    {
        mExplosions.emplace_back(mAtoms[colliders[i] - i].getPosition());
        mScore++;
        mAtoms.erase(mAtoms.begin() + colliders[i] - i);
    }
}
