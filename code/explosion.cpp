#include "explosion.h"

vi::Explosion::Explosion(sf::Vector2f pos)
{
    mLifespan = sf::seconds(2.f);
    mAge = sf::seconds(0.f);
    mCircle.setRadius(40);
    mCircle.setOrigin(mCircle.getRadius(), mCircle.getRadius());
    mCircle.setPosition(pos);
    mCircle.setFillColor(sf::Color(255, 0, 0, 200));
}

vi::Explosion::~Explosion()
{
    //dtor
}

const bool vi::Explosion::update(sf::Time deltaTime)
{
    mAge += deltaTime;

    if(mAge.asSeconds() >= mLifespan.asSeconds())
    {
        return false;
    }

    float gFactor = mAge.asSeconds() / mLifespan.asSeconds();

    if(mAge.asSeconds() < (mLifespan.asSeconds() / 4))              // Phase One: Expansion
    {
        mCircle.setScale(sf::Vector2f(gFactor * 4,
                                      gFactor * 4));
    }
    else if(mLifespan.asSeconds() - mAge.asSeconds() < (mLifespan.asSeconds() / 4))     // Phase Three: Contraction
    {
        float rFactor = gFactor - (3.f / 4.f);
        mCircle.setScale(1.f - (rFactor * 4),
                         1.f - (rFactor * 4));
    }
    else                                                            // Phase Two: Climax
    {
        mCircle.setScale(sf::Vector2f(1.f,1.f));
    }

    return true;
}

const sf::Vector2f vi::Explosion::getPosition() const
{
    return mCircle.getPosition();
}

const float vi::Explosion::getRadius() const
{
    return mCircle.getRadius() * mCircle.getScale().x;
}

const sf::CircleShape vi::Explosion::getCircle() const
{
    return mCircle;
}
