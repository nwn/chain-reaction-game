#include "atom.h"

float vi::Atom::Radius = 7.5f;

vi::Atom::Atom(sf::Vector2f pos, unsigned int dir)
{
    mCircle.setRadius(Radius);
    mCircle.setOrigin(mCircle.getRadius(), mCircle.getRadius());
    mCircle.setPosition(pos);
    mCircle.setFillColor(sf::Color(0, 0, 255, 200));
    mSpeedX = 60;
    mSpeedY = 60;

    dir %=4;
    if(dir == 0)
    {}
    else if(dir == 1)
    {
        mSpeedX = -mSpeedX;
    }
    else if(dir == 2)
    {
        mSpeedX = -mSpeedX;
        mSpeedY = -mSpeedY;
    }
    else if(dir == 3)
    {
        mSpeedY = -mSpeedY;
    }
}

vi::Atom::~Atom()
{
    //dtor
}

void vi::Atom::update(const sf::Time dt, const sf::Vector2f winSize)
{
    float deltaTime = dt.asSeconds();

    mCircle.move(sf::Vector2f(mSpeedX * deltaTime, mSpeedY * deltaTime));

    if(mCircle.getPosition().x - mCircle.getOrigin().x <= 0 || mCircle.getPosition().x + mCircle.getGlobalBounds().width - mCircle.getOrigin().x >= winSize.x)
        mSpeedX = -mSpeedX;
    if(mCircle.getPosition().y - mCircle.getOrigin().y <= 0 || mCircle.getPosition().y + mCircle.getGlobalBounds().height - mCircle.getOrigin().y >= winSize.y)
        mSpeedY = -mSpeedY;
}

const sf::Vector2f vi::Atom::getPosition() const
{
    return mCircle.getPosition();
}

const float vi::Atom::getRadius() const
{
    return mCircle.getRadius();
}

const sf::CircleShape vi::Atom::getCircle() const
{
    return mCircle;
}

